![picture](bit_assets/banner-772x250.png)
## Simple Point Quiz For Woocommerce
Requires at least: 4.9
Tested up to: 5.3.2
Stable tag: 4.9
WC requires at least: 3.0.0
WC tested up to: 4.0.0
---

### Description

Simple Woocommerce Product Suggestion via Point System by Taking Quizzes

Plugin is Translatable ready via PoEdit

Report/Post Bugs here:https://bitbucket.org/allouise/simple-point-quiz-for-woocommerce

Note for Creating Quizzes:
- When both Show Result via Custom Page (Page URL) and Show Result by Choosing Product Result fields are filled the Page URL will take priority

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Create Quizzes from Woocommerce Quiz Questionnaire Tab
4. Create Question & Answers for you Quizzes
5. (Optional) Link Quizzes
6. Copy & Embed Quiz Shortcode

---

### Screenshots

#### Quiz List
![picture](bit_assets/quiz_list.png)

#### Quiz Configuration
![picture](bit_assets/quiz_edit.png)

#### Question Configuration
![picture](bit_assets/question_edit.png)

#### Quiz Frontend
![picture](bit_assets/quiz_front.png)

#### Quiz Result Frontend
![picture](bit_assets/quiz_result_front.png)

---